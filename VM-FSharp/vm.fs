﻿namespace VirtualMachine

exception InvalidOpArgumentException of address : int * message : string

module Arch = 
    let modulo = 32768u

type Number = 
    | Number of value : uint16

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Number = 
    let inline create v = 
        v
        |> uint32
        |> (fun i -> i % Arch.modulo)
        |> uint16
        |> Number
    
    let inline (|+|) (Number(v1)) (Number(v2)) = create (v1 + v2)
    let inline (|*|) (Number(v1)) (Number(v2)) = create (v1 * v2)
    let inline (|%|) (Number(v1)) (Number(v2)) = create (v1 % v2)
    
    let inline (~~~) (Number(v1)) = 
        uint32 (~~~v1) % Arch.modulo
        |> uint16
        |> Number
    
    let inline (&&&) (Number(v1)) (Number(v2)) = Number(v1 &&& v2)
    let inline (|||) (Number(v1)) (Number(v2)) = Number(v1 ||| v2)

module Binary = 
    let (|Value|Register|Invalid|) v = 
        if (v <= uint16 (Arch.modulo - 1u)) then Value(Number.create (v))
        elif (v <= 32775us) then Register(int (32775us - v))
        else Invalid
    
    let toUInt16 offset (bytes : byte []) = 
        let len = bytes.Length
        if (offset >= len || (offset + 1) >= len) then raise <| System.IndexOutOfRangeException()
        let b1 = bytes.[offset] |> uint16
        let b2 = bytes.[offset + 1] |> uint16
        b2 <<< 8 ||| b1

type RegisterMap = Number []

type Vm = 
    { Registers : RegisterMap
      Memory : uint16 []
      Stack : List<uint16>
      Print : char -> unit
      Scan : unit -> int }

type OpCode = 
    | Halt = 0
    | Set = 1
    | Push = 2
    | Pop = 3
    | Eq = 4
    | Gt = 5
    | Jmp = 6
    | Jt = 7
    | Jf = 8
    | Add = 9
    | Mult = 10
    | Mod = 11
    | And = 12
    | Or = 13
    | Not = 14
    | Rmem = 15
    | Wmem = 16
    | Call = 17
    | Ret = 18
    | Out = 19
    | In = 20
    | Noop = 21

module Array = 
    let takeN<'a> startExcl count (array : 'a []) = 
        let adjStart = startExcl + 1
        let offset = count - 1
        if (adjStart >= array.Length) then array
        else if (adjStart + adjStart >= array.Length) then array.[adjStart..]
        else array.[adjStart..adjStart + offset]

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Vm = 
    open Binary
    open Number
    open System.IO
    
    let create memory = 
        { Registers = 
              [| for _ in 0us..7us -> Number.create (0) |]
          Memory = memory
          Stack = []
          Print = System.Console.Write
          Scan = System.Console.Read }
    
    let private invalidOpArg opcode address = 
        InvalidOpArgumentException
            (address, 
             sprintf "Invalid argument or wrong number of arguments provided for operation %A at address %d" opcode 
                 address) |> raise
    
    let load path = 
        let binaryContents = 
            use memoryStream = new MemoryStream(16384)
            use sourceStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None, 16384)
            sourceStream.CopyTo(memoryStream)
            memoryStream.ToArray()
        binaryContents
        |> Array.chunkBySize 2
        |> Array.map (Binary.toUInt16 0)
        |> create
    
    let execute vm = 
        let threeArgsCheck opcode vm address (fn) : int * bool = 
            let memory = vm.Memory
            let registers = vm.Registers
            match memory |> Array.takeN address 3 with
            | [| Register dst; Register a; Value n |] -> (dst, fn registers.[a] n)
            | [| Register dst; Value n1; Value n2 |] -> (dst, fn n1 n2)
            | [| Register dst; Register a; Register b |] -> (dst, fn registers.[a] registers.[b])
            | [| Register dst; Value n; Register b |] -> (dst, fn n registers.[b])
            | _ -> invalidOpArg opcode address
        
        let inline threeArgsSymmetricOp opcode vm address op = 
            match vm.Memory |> Array.takeN address 3 with
            | [| Register dst; Register src; Value n |] | [| Register dst; Value n; Register src |] -> 
                vm.Registers.[dst] <- op vm.Registers.[src] n
            | [| Register dst; Register a; Register b |] -> vm.Registers.[dst] <- op vm.Registers.[a] vm.Registers.[b]
            | [| Register dst; Value n1; Value n2 |] -> vm.Registers.[dst] <- op n1 n2
            | _ -> invalidOpArg opcode address
        
        let rec execute' address vm = 
            if (address >= vm.Memory.Length) then ()
            let opcode : OpCode = 
                vm.Memory.[address]
                |> int
                |> enum
            
            let inline withInvalidOpArg addr = invalidOpArg opcode addr
            let inline threeArgs op = threeArgsCheck opcode vm address op
            let inline threeArgsSymm op = threeArgsSymmetricOp opcode vm address op
            match opcode with
            | OpCode.Halt -> ()
            | OpCode.Set -> 
                match vm.Memory |> Array.takeN address 2 with
                | [| Register i; Value n |] -> vm.Registers.[i] <- n
                | [| Register i; Register v |] -> vm.Registers.[i] <- vm.Registers.[v]
                | _ -> withInvalidOpArg address
                execute' (address + 3) vm
            | OpCode.Push -> 
                let (Number n) = 
                    match vm.Memory |> Array.takeN address 1 with
                    | [| Value n |] -> n
                    | [| Register i |] -> vm.Registers.[i]
                    | _ -> withInvalidOpArg address
                execute' (address + 2) { vm with Stack = n :: vm.Stack }
            | OpCode.Pop -> 
                match vm.Memory |> Array.takeN address 1 with
                | [| Register i |] -> 
                    match vm.Stack with
                    | x :: xs -> 
                        vm.Registers.[i] <- x |> Number.create
                        execute' (address + 2) { vm with Stack = xs }
                    | _ -> invalidOp <| sprintf "Cannot pop from an empty stack at address %d" address
                | _ -> withInvalidOpArg address
            | OpCode.Eq -> 
                let idx, isEqual = threeArgs (=)
                vm.Registers.[idx] <- (if isEqual then 1
                                       else 0)
                                      |> Number.create
                execute' (address + 4) vm
            | OpCode.Gt -> 
                let idx, isGreater = threeArgs (>)
                vm.Registers.[idx] <- (if isGreater then 1
                                       else 0)
                                      |> Number.create
                execute' (address + 4) vm
            | OpCode.Jmp -> 
                match vm.Memory |> Array.takeN address 1 with
                | [| Value(Number n) |] -> execute' (int n) vm
                | _ -> withInvalidOpArg address
            | OpCode.Jt -> 
                let inline jt n1 n2 = 
                    if (n1 <> 0us) then execute' (int n2) vm
                    else execute' (address + 3) vm
                match vm.Memory |> Array.takeN address 2 with
                | [| Register src; Value(Number n) |] -> 
                    let (Number a) = vm.Registers.[src]
                    jt a n
                | [| Value(Number a); Value(Number b) |] -> jt a b
                | _ -> withInvalidOpArg address
            | OpCode.Jf -> 
                let inline jf n1 n2 = 
                    if (n1 = 0us) then execute' (int n2) vm
                    else execute' (address + 3) vm
                match vm.Memory |> Array.takeN address 2 with
                | [| Register src; Value(Number n) |] -> 
                    let (Number a) = vm.Registers.[src]
                    jf a n
                | [| Value(Number a); Value(Number b) |] -> jf a b
                | _ -> withInvalidOpArg address
            | OpCode.Add -> 
                threeArgsSymm (|+|)
                execute' (address + 4) vm
            | OpCode.Mult -> 
                threeArgsSymm (|*|)
                execute' (address + 4) vm
            | OpCode.Mod -> 
                match vm.Memory |> Array.takeN address 3 with
                | [| Register dst; Register src; Value n |] -> vm.Registers.[dst] <- (vm.Registers.[src] |%| n)
                | [| Register dst; Value n; Register src |] -> vm.Registers.[dst] <- (n |%| vm.Registers.[src])
                | [| Register dst; Register a; Register b |] -> 
                    vm.Registers.[dst] <- (vm.Registers.[a] |%| vm.Registers.[b])
                | [| Register dst; Value n1; Value n2 |] -> vm.Registers.[dst] <- (n1 |%| n2)
                | _ -> withInvalidOpArg address
                execute' (address + 4) vm
            | OpCode.And -> 
                threeArgsSymm (&&&)
                execute' (address + 4) vm
            | OpCode.Or -> 
                threeArgsSymm (|||)
                execute' (address + 4) vm
            | OpCode.Not -> 
                match vm.Memory |> Array.takeN address 2 with
                | [| Register dst; Register src |] -> vm.Registers.[dst] <- ~~~vm.Registers.[src]
                | [| Register dst; Value n |] -> vm.Registers.[dst] <- ~~~n
                | _ -> withInvalidOpArg address
                execute' (address + 3) vm
            | OpCode.Rmem -> 
                match vm.Memory |> Array.takeN address 2 with
                | [| Register dst; Value(Number n) |] -> vm.Registers.[dst] <- vm.Memory.[int n] |> Number.create
                | [| Register dst; Register src |] -> 
                    let (Number n) = vm.Registers.[src]
                    vm.Registers.[dst] <- vm.Memory.[int n] |> Number.create
                | _ -> withInvalidOpArg address
                execute' (address + 3) vm
            | OpCode.Wmem -> 
                match vm.Memory |> Array.takeN address 2 with
                | [| Value(Number n); Register dst |] -> 
                    let (Number v) = vm.Registers.[dst]
                    vm.Memory.[int n] <- v
                | [| Register src; Register dst |] -> 
                    let (Number v) = vm.Registers.[dst]
                    let (Number n) = vm.Registers.[src]
                    vm.Memory.[int n] <- v
                | [| Register dst; Value(Number v) |] -> 
                    let (Number n) = vm.Registers.[dst]
                    vm.Memory.[int n] <- v
                | [| Value(Number n); Value(Number v) |] -> vm.Memory.[int n] <- v
                | _ -> withInvalidOpArg address
                execute' (address + 3) vm
            | OpCode.Call -> 
                let (Number n) = 
                    match vm.Memory |> Array.takeN address 1 with
                    | [| Value n |] -> n
                    | [| Register i |] -> vm.Registers.[i]
                    | _ -> withInvalidOpArg address
                execute' (int n) { vm with Stack = uint16 (address + 2) :: vm.Stack }
            | OpCode.Ret -> 
                match vm.Stack with
                | x :: xs -> execute' (int x) { vm with Stack = xs }
                | [] -> ()
            | OpCode.Out -> 
                let (Number v) = 
                    match vm.Memory |> Array.takeN address 1 with
                    | [| Register i |] -> vm.Registers.[i]
                    | [| Value n |] -> n
                    | _ -> withInvalidOpArg address
                v
                |> char
                |> vm.Print
                execute' (address + 2) vm
            | OpCode.In -> 
                match vm.Memory |> Array.takeN address 1 with
                | [| Register dst |] -> 
                    let mutable c = vm.Scan()
                    while char (c) = '\r' do
                        c <- vm.Scan()
                    vm.Registers.[dst] <- c |> Number.create
                | _ -> withInvalidOpArg address
                execute' (address + 2) vm
            | OpCode.Noop -> execute' (address + 1) vm
            | other -> 
                System.NotSupportedException(sprintf "Unrecognized opcode value %d at address %d" (int other) address) 
                |> raise
        
        execute' 0 vm
    
    let run path = 
        path
        |> load
        |> execute
