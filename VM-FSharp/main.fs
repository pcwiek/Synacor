﻿namespace VirtualMachine

module Main = 
    [<EntryPoint>]
    let main args = 
        try
            args
            |> Array.tryFind (fun _ -> true)
            |> Option.fold (fun _ v -> v) "challenge.bin"
            |> Vm.run
        with
        | ex -> 
            printfn "%A" ex
            printfn "%A" ex.StackTrace
        System.Console.WriteLine()
        1
