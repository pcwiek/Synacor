mod vm {
    use std::mem;
    use std::ops;
    use std::io;
    use std::fs;
    use std::io::Read;
    use std::fmt;
    use std::error;
    
    #[derive(Debug)]
    pub enum RuntimeError {
        InvalidOpArgument(OpCode, u16),
        InvalidOperation(String),
        UnsupportedOpcode(u16)
    }

    impl fmt::Display for RuntimeError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match *self {
                RuntimeError::InvalidOpArgument(ref opcode, address) => write!(f, "Invalid argument or wrong number of arguments provided for operation {:?} at address {}", opcode, address),
                RuntimeError::InvalidOperation(ref msg) => write!(f, "{}", msg),
                RuntimeError::UnsupportedOpcode(raw_opcode) => write!(f, "Unsupported opcode value {}", raw_opcode)
            }
        }
    }
    impl error::Error for RuntimeError {
        fn description(&self) -> &str {
            match *self {
                RuntimeError::InvalidOpArgument(_,_) => "Invalid operation argument",
                RuntimeError::InvalidOperation(_) => "Invalid operation",
                RuntimeError::UnsupportedOpcode(_) => "Unsupported opcode"
            }
        }

        fn cause(&self) -> Option<&error::Error> {
            None
        }
    }

    #[derive(Debug)]
    #[repr(u8)]
    pub enum OpCode {
        Halt,
        Set,
        Push,
        Pop,
        Eq,
        Gt,
        Jmp,
        Jt,
        Jf,
        Add,
        Mult,
        Mod,
        And,
        Or,
        Not,
        Rmem,
        Wmem,
        Call,
        Ret,
        Out,
        In,
        Noop
    }
    
    impl OpCode {
        pub fn from_u8(n : u8) -> Option<OpCode> {
            if n <= 21 {
                Some(unsafe { mem::transmute(n) })
            } else {
                None
            }
        }
    }

    pub mod arch {
        pub const MOD : u32 = 32768;
    }

    #[derive(Debug, Copy, Clone, Hash, PartialEq, PartialOrd, Ord, Eq)]
    struct Number{
        value: u16
    }

    impl Number {
        pub fn new(v : u16) -> Number {
            Number::from(v as u32)
        }
    }

    impl From<u32> for Number {
        fn from(value : u32) -> Number {
            Number{ value: (value % arch::MOD) as u16 }
        }
    }
    impl From<i32> for Number {
         fn from(value : i32) -> Number {
            Number{ value: ((value as u32) % arch::MOD) as u16 }
        }
    }

    impl ops::Add for Number {
        type Output = Number;
        fn add(self, rhs : Number) -> Number {
            Number::from(self.value as u32 + rhs.value as u32)
        }
    }
    impl ops::Mul for Number {
        type Output = Number;
        fn mul(self, rhs : Number) -> Number {
            Number::from(self.value as u32 * rhs.value as u32)
        }
    }
    impl ops::Not for Number {
        type Output = Number;

        fn not(self) -> Number {
            Number::new(!self.value)
        }
    }
    impl ops::BitAnd for Number {
        type Output = Number;

        fn bitand(self, rhs : Number) -> Number {
            Number::new(self.value & rhs.value)
        }
    }
    impl ops::BitOr for Number {
        type Output = Number;

        fn bitor(self, rhs : Number) -> Number {
            Number::new(self.value | rhs.value)
        }
    }
    impl ops::Rem for Number {
        type Output = Number;
        fn rem(self, rhs : Number) -> Number {
            Number::new(self.value % rhs.value)
        }
    }

    #[derive(Debug)]
    enum Binary {
        Value(Number),
        Register(i32),
        Invalid
    }

    impl Binary {
        fn from_u16(v : u16) -> Binary {
            if v <= (arch::MOD - 1) as u16 { Binary::Value(Number::new(v)) } 
            else if v <= 32775 {
                Binary::Register(32775 - v as i32)
            } else {
                Binary::Invalid
            }
        }

        fn to_u16(bytes : [u8; 2]) -> u16 {
            (bytes[1] as u16) << 8 | (bytes[0] as u16)
        }
    }

    type RegisterMap = [Number;8];

    #[derive(Debug)]
    pub struct Vm {
        registers : RegisterMap,
        memory : Vec<u16>,
        stack : Vec<u16>,
    }

    impl Vm {
        fn new(mem : Vec<u16>) -> Vm {
            Vm {
                registers: [Number::new(0); 8],
                memory: mem,
                stack: Vec::new(),
            }
        }

        fn print(code : u8) {
            print!("{}", code as char)
        }

        fn scan() -> Option<u8> {
            let stdin = io::stdin();
            let mut bytes = stdin.lock().bytes();
            let mut get_next_char_iter = || bytes
                                             .next()
                                             .into_iter()
                                             .filter_map(|c| c.ok());
            let character = 
                get_next_char_iter()
                    .filter(|c| *c != '\r' as u8)
                    .next()
                    .or_else(|| get_next_char_iter().next());
            character
        }

        // Perfect candidate for macros, I guess
        fn extract_1(&self, addr : usize) -> Binary {
            Binary::from_u16(self.memory[addr + 1])
        }
        fn extract_2(&self, addr : usize) -> (Binary,Binary) {
            (Binary::from_u16(self.memory[addr + 1]), Binary::from_u16(self.memory[addr + 2]))
        }
        fn extract_3(&self, addr : usize) -> (Binary,Binary,Binary) {
            (Binary::from_u16(self.memory[addr + 1]), Binary::from_u16(self.memory[addr + 2]), Binary::from_u16(self.memory[addr + 3]))
        }
        //----------------------------

        fn get_underlying_number(&self, address : usize) -> Option<Number> {
            match self.extract_1(address) {
                Binary::Value(n) => Some(n),
                Binary::Register(i) => Some(self.registers[i as usize]),
                _ => None
            }
        }

        fn three_args(&self, address : usize) -> Option<(usize, Number, Number)> {
            match self.extract_3(address) {
                (Binary::Register(dst), Binary::Register(a), Binary::Value(n)) => Some((dst as usize, self.registers[a as usize], n)),
                (Binary::Register(dst), Binary::Value(n1), Binary::Value(n2)) => Some((dst as usize, n1, n2)),
                (Binary::Register(dst), Binary::Register(a), Binary::Register(b)) => Some((dst as usize, self.registers[a as usize], self.registers[b as usize])),
                (Binary::Register(dst), Binary::Value(n), Binary::Register(b)) => Some((dst as usize, n, self.registers[b as usize])),
                _ => None
            }
        }

        fn load(path : &str) -> Result<Vm, io::Error> {
            let mut f = try!(fs::File::open(path));
            let mut buffer = Vec::with_capacity(16000);
            try!(f.read_to_end(&mut buffer));
            let mem = buffer.chunks(2).map(|chunk| Binary::to_u16([chunk[0], chunk[1]])).collect::<Vec<_>>();
            Ok(Vm::new(mem))
        }

        fn execute(&mut self) -> Result<(), RuntimeError> {
            let mut address : usize = 0;
            while address < self.memory.len() {
                match OpCode::from_u8(self.memory[address] as u8) {
                    Some(opcode) => match opcode {
                        OpCode::Halt => return Ok(()),
                        OpCode::Set => {
                            match self.extract_2(address) {
                                (Binary::Register(i), Binary::Value(n)) => self.registers[i as usize] = n,
                                (Binary::Register(i), Binary::Register(v)) =>  self.registers[i as usize] = self.registers[v as usize],
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            }
                            address += 3;
                        },
                        OpCode::Push => {
                            let n = try!(self.get_underlying_number(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.stack.push(n.value);
                            address += 2;
                        },
                        OpCode::Pop => {
                            match self.extract_1(address) {
                                Binary::Register(i) => {
                                    if let Some(x) = self.stack.pop() {
                                        self.registers[i as usize] = Number::new(x);
                                        address += 2;
                                    } else {
                                        return Err(RuntimeError::InvalidOperation(format!("Cannot pop from an empty stack at address {}", address)))
                                    }
                                },
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            }
                        },
                        OpCode::Eq => {
                            let (dst, a, b) = try!(self.three_args(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.registers[dst] = Number::new(if a == b { 1 } else { 0 });
                            address += 4;
                        },
                        OpCode::Gt => {
                            let (dst, a, b) = try!(self.three_args(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.registers[dst] = Number::new(if a > b { 1 } else { 0 });
                            address += 4;
                        },
                        OpCode::Jmp => {
                            match self.extract_1(address) {
                                Binary::Value(n) => address = n.value as usize,
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            }
                        },
                        OpCode::Jt => {
                            let (n1, n2) = match self.extract_2(address) {
                                (Binary::Register(src), Binary::Value(n)) => (self.registers[src as usize], n),
                                (Binary::Value(n1), Binary::Value(n2)) => (n1, n2),
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            };
                            if n1.value != 0 {
                                address = n2.value as usize;
                            } else {
                                address += 3;
                            }
                        },
                        OpCode::Jf => {
                            let (n1, n2) = match self.extract_2(address) {
                                (Binary::Register(src), Binary::Value(n)) => (self.registers[src as usize], n),
                                (Binary::Value(n1), Binary::Value(n2)) => (n1, n2),
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            };
                            if n1.value == 0 {
                                address = n2.value as usize;
                            } else {
                                address += 3;
                            }
                        },
                        OpCode::Add => {
                            let (dst, a, b) = try!(self.three_args(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.registers[dst] = a + b;
                            address += 4;
                        },
                        OpCode::Mult => {
                            let (dst, a, b) = try!(self.three_args(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.registers[dst] = a * b;
                            address += 4;
                        },
                        OpCode::Mod => {
                            let (dst, a, b) = try!(self.three_args(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.registers[dst] = a % b;
                            address += 4;
                        },
                        OpCode::And => {
                            let (dst, a, b) = try!(self.three_args(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.registers[dst] = a & b;
                            address += 4;
                        },
                        OpCode::Or => {
                            let (dst, a, b) = try!(self.three_args(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.registers[dst] = a | b;
                            address += 4;
                        },
                        OpCode::Not => {
                            match self.extract_2(address) {
                                (Binary::Register(dst), Binary::Register(src)) => self.registers[dst as usize] = !self.registers[src as usize],
                                (Binary::Register(dst), Binary::Value(n)) => self.registers[dst as usize] = !n,
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            }
                            address += 3;
                        },
                        OpCode::Rmem => {
                            match self.extract_2(address) {
                                (Binary::Register(dst), Binary::Value(n)) => self.registers[dst as usize] = Number::new(self.memory[n.value as usize]),
                                (Binary::Register(dst), Binary::Register(src)) => {
                                    let n = self.registers[src as usize];
                                    self.registers[dst as usize] = Number::new(self.memory[n.value as usize]);
                                },
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            }
                            address += 3;
                        },
                        OpCode::Wmem => {
                            let (n1, n2) = match self.extract_2(address) {
                                (Binary::Value(n), Binary::Register(dst)) => (n, self.registers[dst as usize]),
                                (Binary::Register(src), Binary::Register(dst)) => (self.registers[src as usize], self.registers[dst as usize]),
                                (Binary::Register(dst), Binary::Value(v)) => (self.registers[dst as usize], v),
                                (Binary::Value(n), Binary::Value(v)) => (n, v),
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            };
                            self.memory[n1.value as usize] = n2.value;
                            address += 3;
                        },
                        OpCode::Call => {
                            let n = try!(self.get_underlying_number(address).ok_or_else(|| RuntimeError::InvalidOpArgument(opcode, address as u16)));
                            self.stack.push((address + 2) as u16);
                            address = n.value as usize;
                        },
                        OpCode::Ret => {
                            if let Some(x) = self.stack.pop() {
                                address = x as usize;
                            } else {
                                return Ok(());
                            }
                        },
                        OpCode::Out => {
                            let n = match self.extract_1(address) {
                                Binary::Register(i) => self.registers[i as usize],
                                Binary::Value(v) => v,
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            };
                            Vm::print(n.value as u8);
                            address += 2;
                        },
                        OpCode::In => {
                            match self.extract_1(address) {
                                Binary::Register(i) => {
                                    let r = Vm::scan().unwrap();
                                    self.registers[i as usize] = Number::new(r as u16);
                                },
                                _ => return Err(RuntimeError::InvalidOpArgument(opcode, address as u16))
                            }
                            address += 2;
                        },
                        OpCode::Noop => address += 1,
                    },
                    None => return Err(RuntimeError::UnsupportedOpcode(self.memory[address as usize]))
                }
            }     
            Ok(())       
        }
    }

    pub fn run_from_file(path : &str) -> Result<(), Box<error::Error>> {
        let mut vm = try!(Vm::load(path));
        let _ = try!(vm.execute());
        Ok(())
    }
}

fn main() {
    vm::run_from_file("challenge.bin").unwrap();
}
